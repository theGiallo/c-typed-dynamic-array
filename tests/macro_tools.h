
#undef GLUE
#undef GLUE2
#undef GLUE3_
#undef GLUE3
#undef STRINGIFY
#undef TOSTRING
#undef HERE

#define GLUE(x,y) x##y
#define GLUE2(x,y) GLUE(x,y)
#define GLUE3_(x,y,z) x##y##z
#define GLUE3(x,y,z) GLUE3_(x,y,z)
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define HERE() __FILE__ ":" TOSTRING(__LINE__)