#pragma once

#ifndef __cplusplus
typedef struct Foo_st Foo;
struct Foo_st
{
	int bar;
	Foo * a_foo;
};
#else
struct Foo
{
	int bar;
	Foo * a_foo;
};
#endif