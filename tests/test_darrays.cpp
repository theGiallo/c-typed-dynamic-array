#include "darrays.h"
#include "foo.h"
#include "macro_tools.h"

#include <cstdio>
#include <cstring>

#define CNSL_RESET "\e[00m"
#define CNSL_RED "\e[0;31m"

#define log_info(format, ...) printf(format "\n", __VA_ARGS__)
#define log_err(format, ...) fprintf(stderr, CNSL_RED format CNSL_RESET " @" HERE() "\n", ##__VA_ARGS__)

using namespace tg;

int main(int argc, char const *argv[])
{
	char_DArray a_string = {};

	if (!a_string.push("A"))
	{
		log_err("Realloc error in char_DArray_push!");
	}
	log_info( "a_string.length     = %d", a_string.length     );
	log_info( "a_string.max_length = %d", a_string.max_length );
	if (!a_string.push("B"))
	{
		log_err("Realloc error in char_DArray_push!");
	}
	log_info( "a_string.length     = %d", a_string.length     );
	log_info( "a_string.max_length = %d", a_string.max_length );

	a_string.data[a_string.length] = '\0';
	log_info( "a_string.data: '%s'", a_string.data );

	const char * good_old_c_string = "OK, now let's do something more interesting!";
	size_t len = 1 + strlen(good_old_c_string);

	// NOTE: this is for efficiency
	if (!_char_DA_check_realloc(&a_string, len))
	{
		log_err("Realloc error in _char_DA_check_realloc!");
	}
	memcpy(a_string.data, good_old_c_string, len);

	log_info( "a_string.length     = %d", a_string.length     );
	log_info( "a_string.max_length = %d", a_string.max_length );
	log_info( "a_string.data: '%s'",      a_string.data );
	// NOTE: remember to update length!
	a_string.length = len;
	log_info( "a_string.length     = %d", a_string.length     );
	log_info( "a_string.max_length = %d", a_string.max_length );

	a_string.free();
	// NOTE: you can use a DArray after you have freed it

	// NOTE: you can also do many pushes
	for (int i=0; i!=len; ++i)
	{
		a_string.push(good_old_c_string+i);
	}
	// NOTE: in this way you don't need to update length manually,
	// but it is more efficient to use memcpy
	log_info( "a_string.data: '%s'",      a_string.data );
	log_info( "a_string.length     = %d", a_string.length     );
	log_info( "a_string.max_length = %d", a_string.max_length );

	a_string.free();

	Foo_DArray f_a = {};
	Foo f = {1, nullptr};
	f_a.push(&f);
	f_a.push_empty();
	log_info( "f_a.length     = %d", f_a.length     );
	log_info( "f_a.max_length = %d", f_a.max_length );
	f_a.free();

	return 0;
}
