#!/bin/bash

cp ../clean_src/darray_typed.inc.h .
cp ./darrays.cpp darrays.c

echo -e "\nCompiling c++ test..."
g++ -fdiagnostics-color=auto -ftime-report -std=c++11 -DDEBUG=1 darrays.cpp test_darrays.cpp -o test_darrays_cpp
res=$?
if [ $res -eq 0 ]
then
	echo -e "\nc++ version compiled."
else
	echo -e "\nc++ version compiling failed."
fi

echo -e "\nCompiling c11 test..."
gcc -fdiagnostics-color=auto -ftime-report -std=c11 -DDEBUG=1 darrays.c test_darrays.c -o test_darrays_c11
res=$?
if [ $res -eq 0 ]
then
	echo -e "\nc11 version compiled."
else
	echo -e "\nc11 version compiling failed."
fi

echo -e "\nCompiling c99 test..."
gcc -fdiagnostics-color=auto -ftime-report -std=c99 -DDEBUG=1 darrays.c test_darrays.c -o test_darrays_c99
res=$?
if [ $res -eq 0 ]
then
	echo -e "\nc99 version compiled."
else
	echo -e "\nc99 version compiling failed."
fi

exit $?
