#pragma once

#if !DA_EMIT_BODY
	#define DA_EMIT_HEADER 1
#endif

#define DA_TYPE char
#include "darray_typed.inc.h"

#include "foo.h"
#define DA_TYPE Foo
#include "darray_typed.inc.h"
