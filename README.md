# C Typed Dynamic Array

Practically speaking this is a C version of templates. (done with macros)

If you use it in C++ it comes inside a namespace named `tg` and it has methods. No destructor nor constructor.

Single file library. You can use custom allocators and make functions static.

Dependencies: `free`, `realloc`, `memcpy`, but you can define and use your own ones.

## Why?
- If you don't like templates because they are slow to compile and difficult to debug.
- If you don't like the angle brackets.
- If you don't like that the container type comes first.
- If you code in C.
- If you want type checking and explicit container type.

## How to use them?
Give a look at the tests folder and at the documentation hereafter.

## Documentation from darray_typed.inc.h:

If c++ all will be inside a namespace named "tg".
Methods are there only if c++.
No destructor nor constructor.
If you want the functions to be static define DA_STATIC to 1

If you need a dynamic array of a certain type (e.g. my_type_t)
define DA_TYPE to that typename and include this file:

    #define DA_TYPE my_type_t
    #include "darray_typed.inc.h"

A struct type and some utility functions will be created.

The dynamic array will be a

    struct my_type_t_DArray
    {
    	my_type_t * data;
    	DA_SIZE_T   max_length,
    	            length;
    	void * alloc_context;
    }

A typedef defines the types 'my_type_t_DArray' and 'my_type_t_DA'.

IMPORTANT: You should not modify max_length nor data.
You may modify length, keeping length <= max_length.

To create a new instance of it initialize it to all 0s:

    my_type_y_DArray my_da = {};
    // (if you're std is < c/c++11 use {0})

If you want to use a custom allocator that has a context assign it:

    my_da.alloc_context = &my_allocation_context;

To use a custom allocation system you have to define these two macros:

    #define DA_REALLOC(a,b,c) my_realloc(a,b,c)
    #define DA_FREE(a,c) my_free(a,c)

where a is a pointer, b is a size and c the allocation context.
If you don't define them they will default to std realloc and free,
and stdlib will be included.

DA_SIZE_T defaults to size_t with the inclusion of stdlib, so if you don't
want stdlib you can define DA_SIZE_T before the inclusion of this file.

To add an element to the array you have 2 functions.

    bool
    my_type_t_DA_push      ( my_type_t_DArray * a, const my_type_t * e )
    bool
    my_type_t_DA_push_empty( my_type_t_DArray * a )

or methods

    bool
    push      ( const my_type_t * e )
    bool
    push_empty()

\*_DA_push will copy the given element using DA_MEMCPY, that defaults to
std memcpy with the inclusion of std string. If you don't want it to be
included you can define it before including this file.

\*_DA_push_empty will set to 0 all the memory of the newly created element.

A clean DArray is all set to 0. When you push an element into a clean
DArray will be allocated memory for 16 elements. When you push an element
into a full DArray (length == max_length) its memory will be reallocated
doubling its previous size.

These functions return true on success and false if there was an error
during reallocation.

To access and remove the last element use

    my_type_t *
    my_type_t_DA_pop( my_type_t_DArray * a )
    
or method
    
    my_type_t *
    pop()

The pointer returned points into the array, at data[length].

IMPORTANT: the array could be moved during push, so you may want to copy
the element somewhere safe.

To access to an element use

    data[index]

with 0 <= index < length .

To free the memory of the dynamic array use

    void
    my_type_t_DA_free(my_type_t_DArray * a)

	or method

    void
    free()

### Example

    #if !DA_EMIT_BODY
    	#define DA_EMIT_HEADER 1
    #endif
    
    #define DA_TYPE char
    #include "darray_typed.inc.h"

## License
zlib license! Be free!

    Copyright (c) <2015-2016> <Gianluca Alloisio>

    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

       1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.

       2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.

       3. This notice may not be removed or altered from any source
       distribution.

    
