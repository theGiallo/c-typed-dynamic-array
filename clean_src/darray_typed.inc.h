/*

Copyright (c) <2015-2016> <Gianluca Alloisio>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

*/
/*
                              C Typed Dynamic Array                             

	If c++ all will be inside a namespace named "tg".
	Methods are there only if c++.
	No destructor nor constructor.
	If you want the functions to be static define DA_STATIC to 1

	If you need a dynamic array of a certain type (e.g. my_type_t)
	define DA_TYPE to that typename and include this file:

		#define DA_TYPE my_type_t
		#include "darray_typed.inc.h"

	A struct type and some utility functions will be created.

	The dynamic array will be a

		struct my_type_t_DArray
		{
			my_type_t * data;
			DA_SIZE_T   max_length,
			            length;
			void * alloc_context;
		}

	A typedef defines the types 'my_type_t_DArray' and 'my_type_t_DA'.

	IMPORTANT: You should not modify max_length nor data.
	You may modify length, keeping length <= max_length.

	To create a new instance of it initialize it to all 0s:

		my_type_y_DArray my_da = {};
		// (if you're std is < c/c++11 use {0})
	
	If you want to use a custom allocator that has a context assign it:

		my_da.alloc_context = &my_allocation_context;

	To use a custom allocation system you have to define these two macros:

		#define DA_REALLOC(a,b,c) my_realloc(a,b,c)
		#define DA_FREE(a,c) my_free(a,c)
	
	where a is a pointer, b is a size and c the allocation context.
	If you don't define them they will default to std realloc and free,
	and stdlib will be included.
	DA_SIZE_T defaults to size_t with the inclusion of stdlib, so if you don't
	want stdlib you can define DA_SIZE_T before the inclusion of this file.

	To add an element to the array you have 2 functions.

		bool
		my_type_t_DA_push      ( my_type_t_DArray * a, const my_type_t * e )
		bool
		my_type_t_DA_push_empty( my_type_t_DArray * a )

		or methods

		bool
		push      ( const my_type_t * e )
		bool
		push_empty()

		*_DA_push will copy the given element using DA_MEMCPY, that defaults to
		std memcpy with the inclusion of std string. If you don't want it to be
		included you can define it before including this file.
		*_DA_push_empty will set to 0 all the memory of the newly created element.

		A clean DArray is all set to 0. When you push an element into a clean
		DArray will be allocated memory for 16 elements. When you push an element
		into a full DArray (length == max_length) its memory will be reallocated
		doubling its previous size.

		These functions return true on success and false if there was an error
		during reallocation.

	To access and remove the last element use

		my_type_t *
		my_type_t_DA_pop( my_type_t_DArray * a )

		or method

		my_type_t *
		pop()

		The pointer returned points into the array, at data[length].
		IMPORTANT: the array could be moved during push, so you may want to copy
		the element somewhere safe.

	To access to an element use

		data[index]

	with 0 <= index < length .

	To free the memory of the dynamic array use

		void
		my_type_t_DA_free(my_type_t_DArray * a)

		or method

		void
		free()
 */

/*
	NOTE(theGiallo): example

#if !DA_EMIT_BODY
	#define DA_EMIT_HEADER 1
#endif

#define DA_TYPE char
#include "darray_typed.inc.h"

*/

#ifndef DA_TYPE
	#error You must define DA_TYPE to the type you want the DArray to store.
#endif
#if !DA_EMIT_HEADER && !DA_EMIT_BODY
	#error You must define to 1 DA_EMIT_HEADER or DA_EMIT_BODY
#endif

#if DA_STATIC
	#define DA_DEF static
#else
	#define DA_DEF extern
#endif

// NOTE(theGiallo): macro tools
#ifndef _DA_MACRO_TOOLS
#define _DA_MACRO_TOOLS
#define _DA_GLUE_(x,y) x##y
#define _DA_GLUE2_(x,y) _DA_GLUE_(x,y)
#define _DA_GLUE_3_(x,y,z) x##y##z
#define _DA_GLUE3_(x,y,z) _DA_GLUE_3_(x,y,z)
#define _DA_STRINGIFY_(x) #x
#define _DA_TOSTRING__(x) _DA_STRINGIFY_(x)
#define _DA_HERE_() __FILE__ ":" _DA_TOSTRING_(__LINE__)
#endif

#define MY_TYPE _DA_GLUE2_(DA_TYPE, _DArray)
#define MY_TYPE_SHORT _DA_GLUE2_(DA_TYPE, _DA)
#define MY_TYPE_LONG _DA_GLUE2_(DA_TYPE, _DArray)

#if DA_EMIT_HEADER

#ifndef __cplusplus
	#ifndef bool
	#define bool unsigned char
	#endif
	#ifndef true
	#define true (1==1)
	#endif
	#ifndef false
	#define false (!true)
	#endif
#endif

#ifndef DA_SIZE_T
	#define DA_SIZE_T size_t

	#ifdef __cplusplus
	#include <cstdlib>
	#else
	#include <stdlib.h>
	#endif

#endif

#ifdef __cplusplus
namespace tg
{
#endif
	typedef struct MY_TYPE_LONG MY_TYPE;
	typedef MY_TYPE MY_TYPE_SHORT;

	DA_DEF
	bool
	_DA_GLUE3_(_,DA_TYPE,_DA_check_realloc)(MY_TYPE * a, DA_SIZE_T count);
	DA_DEF
	bool
	_DA_GLUE3_(_,DA_TYPE, _DA_check_realloc_one)(MY_TYPE * a);

	DA_DEF
	bool
	_DA_GLUE2_(DA_TYPE, _DA_push)(MY_TYPE * a, const DA_TYPE * e);
	DA_DEF
	bool
	_DA_GLUE2_(DA_TYPE, _DA_push_empty)(MY_TYPE * a);

	DA_DEF
	DA_TYPE *
	_DA_GLUE2_(DA_TYPE, _DA_pop)(MY_TYPE * a);
	DA_DEF
	void
	_DA_GLUE2_(DA_TYPE, _DA_free)(MY_TYPE * a);

	struct
	MY_TYPE_LONG
	{
		DA_TYPE * data;
		DA_SIZE_T length,
		          max_length;
		void * alloc_context;

		#ifdef __cplusplus
		bool
		push(const DA_TYPE * e)
		{
			return _DA_GLUE2_(DA_TYPE, _DA_push)(this, e);
		}

		bool
		push_empty()
		{
			return _DA_GLUE2_(DA_TYPE, _DA_push_empty)(this);
		}

		DA_TYPE *
		pop()
		{
			return _DA_GLUE2_(DA_TYPE, _DA_pop)(this);
		}

		void
		free()
		{
			_DA_GLUE2_(DA_TYPE, _DA_free)(this);
		}


		bool
		_check_realloc(DA_SIZE_T count)
		{
			return _DA_GLUE3_(_,DA_TYPE,_DA_check_realloc)(this, count);
		}
		inline
		bool
		_check_realloc_one()
		{
			return _check_realloc( length + 1 );
		}
		#endif
	};

#ifdef __cplusplus
} // namespace
#endif
#endif
#if DA_EMIT_BODY

#if !defined DA_MEMCPY
	#define DA_MEMCPY(dst,src,size) memcpy(dst,src,size)

	#ifdef __cplusplus
	#include <cstring>
	#else
	#include <string.h>
	#endif

#endif

#if ( defined DA_REALLOC && ! defined DA_FREE ) || ( ! defined DA_REALLOC && defined DA_FREE )
	#error You must define either none of both DA_REALLOC and DA_FREE
#endif

#if !defined DA_REALLOC
	#define DA_REALLOC(a,b,c) realloc(a,b)
	#define DA_FREE(a,c)    free(a)

	#ifdef __cplusplus
	#include <cstdlib>
	#else
	#include <stdlib.h>
	#endif

#endif

#ifdef __cplusplus
namespace tg
{
#endif

	bool
	_DA_GLUE3_(_,DA_TYPE,_DA_check_realloc)(MY_TYPE * a, DA_SIZE_T count)
	{
		if (a->max_length >= count)
		{
			return true;
		}

		DA_SIZE_T new_max_length = a->max_length;
		if (!new_max_length)
		{
			new_max_length = 16;
		}
		while (new_max_length < count)
		{
			new_max_length <<= 1;
		}

		void * new_data = DA_REALLOC(a->data, new_max_length*sizeof(DA_TYPE), a->alloc_context);
		if (new_data)
		{
			a->data = (DA_TYPE *)new_data;
			a->max_length = new_max_length;
			return true;
		}

		return false;
	}

	inline
	bool
	_DA_GLUE3_(_,DA_TYPE, _DA_check_realloc_one)(MY_TYPE * a)
	{
		return _DA_GLUE3_(_,DA_TYPE,_DA_check_realloc)(a, a->length+1);
	}

	bool
	_DA_GLUE2_(DA_TYPE, _DA_push)(MY_TYPE * a, const DA_TYPE * e)
	{
		if (!_DA_GLUE3_(_,DA_TYPE,_DA_check_realloc_one)(a))
		{
			return false;
		}

		DA_MEMCPY(a->data + a->length++, e, sizeof(DA_TYPE));
		return true;
	}
	bool
	_DA_GLUE2_(DA_TYPE, _DA_push_empty)(MY_TYPE * a)
	{
		if (!_DA_GLUE3_(_,DA_TYPE,_DA_check_realloc_one)(a))
		{
			return false;
		}

		memset(a->data + a->length++, 0x0, sizeof(DA_TYPE));
		return true;
	}
	DA_TYPE *
	_DA_GLUE2_(DA_TYPE, _DA_pop)(MY_TYPE * a)
	{
		if (!a->length)
		{
			return NULL;
		}

		--a->length;
		DA_TYPE * res =  a->data + a->length;
		return res;
	}
	void
	_DA_GLUE2_(DA_TYPE, _DA_free)(MY_TYPE * a)
	{
		if (a->data)
		{
			DA_FREE(a->data, a->alloc_context);
		}
		a->data = NULL;
		a->length =
		a->max_length = 0;
	}

#ifdef __cplusplus
} // namespace
#endif
#endif


#undef MY_TYPE
#undef DA_TYPE
